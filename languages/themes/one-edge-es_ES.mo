��          �      �       H  
   I     T     W     e     q  �  z     k     w  
   ~     �  	   �     �     �    �     �     �     �     �     �  4       9     G     P     ^  	   o     y     �                     	             
                               % comments ,  Blog post: %s No comments One Edge One Edge is a friendly, elegant and professional WordPress theme, perfect for small businesses, startups, or corporate websites. It comes with a colorful interface that fits happy and innovative people. One Edge has an one-page layout and parallax effect. The theme has a responsive design, a blog section, WooCommerce integration, SEO optimization, and is translation ready. One Edge is highly customizable, so you can easily change anything you want related to its appearance and functionality. One comment Pages: Posted in  Read more %s ... ThemeIsle about  http://themeisle.com/ PO-Revision-Date: 2016-11-12 12:53:19+0000
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=n != 1;
X-Generator: GlotPress/2.3.0-alpha
Language: es
Project-Id-Version: Themes - One Edge
 % comentarios ,  Entrada de blog: %s Sin comentarios One Edge One Edge es un tema de WordPress amable, elegante y profesional, ideal para pequeñas empresas, nuevas empresas o sitios web corporativos. Viene con una interfaz de colores que se adapta a la gente feliz e innovadoras. One Edge tiene un diseño de una página y efecto parallax. El tema es responsive, tiene una sección para blog, la integración WooCommerce, optimización SEO, y está preparado para ser traducido. One Edge es altamente personalizable, por lo que puede cambiar fácilmente cualquier cosa que desees con respecto a su apariencia y funcionalidad. Un comentario Págnas: Publicado en  Leer más %s ... ThemeIsle Sobre  http://themeisle.com/ 